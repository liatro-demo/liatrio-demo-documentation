---
layout: post
title:  "Infrastructure Provisioning"
categories: documentation
---
## Infrastructure Provisioning
### Prerequisites
 A few considerations if you do plan on running locally:
 
    1. you will need Terraform 1.2.9 installed
    2. you will need aws cli installed
    3. ensure APP_NAME and DOCKER_REGISTRY variables are set correctly within your .gitlab-ci.yml file
    4. kubernetes.tf file needs to reflect your ECR registry where you haver your app stored
    5. kubectl needs to be installed

### SETUP ECR Repository
    1. Fork liatro-demo-ecr into your account.
    2. Add **AWS_ACCESS_KEY_ID** and **AWS_SECRET_ACCESS_KEY** to your CICD variables
    3. ensure APP_NAME and DOCKER_REGISTRY variables are set correctly within your .gitlab-ci.yml file
The pipeline is setup to only run when one of the js files or dockerfile is updated. 
Do not continue to the next step until you have your ECR repositry properly setup.

### Creating infrastructure in your account
Infrastructure provision is done using Terraform, If you would like to create the infrastructure simply run the following commands from your terminal: 
{% highlight shell %} 
git clone git@gitlab.com:natejswenson/liatrio-demo.git
cd liatrio-demo
terraform init 
terraform plan
terraform apply
{% endhighlight %}

After terraform apply is comple you will see the following in the terminal:
{% highlight yaml %} 
users:
- name: eks_test-eks-cluster-xxxxx
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "test-eks-cluster-xxxxxx"

EOT
region = "us-east-1"
{% endhighlight %}

You need to copy the name and use it to configure your kubectl as shown below:
{% highlight yaml %}
aws eks --region us-east-1 update-kubeconfig --name eks_test-eks-cluster-xxxxx
{% endhighlight %}

Once you have done this you will able to grab the expoxed API endpoint by running the following in the terminal:
{% highlight shell %}
➜  liatrio-demo git:(main) kubectl get services -A
NAMESPACE     NAME         TYPE           CLUSTER-IP  EXTERNAL-IP                                                               PORT(S)          AGE
default       kubernetes   ClusterIP      xxx.xx.0.1     <none>                                                                    443/TCP          11m
kube-system   kube-dns     ClusterIP      xxx.xx.0.10    <none>                                                                    53/UDP,53/TCP    11m
liatrio       nginx        LoadBalancer   xx.xx.181.3   xxxxx-xxxx.us-east-1.elb.amazonaws.com   5000:30238/TCP   5m11s
{% endhighlight %}

Take the address listed under External-IP and add the suffix :5000/api/msg to hit the api endpoint.

When you are ready to decomission you servies you can run the following command:
{% highlight shell %} 
terraform destroy
{% endhighlight %}

