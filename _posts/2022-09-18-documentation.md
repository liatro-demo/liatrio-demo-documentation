---
layout: post
title:  "Proposed Solution"
categories: documentation
---
## Infrasturcture
AWS was choosen to host the infrastructure, the choice was based on my comfort level with it. The solution shown below could be replicated on GCP or Azure with not alot of effort.
### EKS
Kubbernetes cluster: test-eks-cluster-xxxxx
- CoreDNS
-liatro (Application)

### AWS ECR
JS Application with exposed api accecable from the liatro loadbalancer: 
   {% highlight json %} 
   :5000/api/msg ->  “message”: “Automate all the things!”“timestamp”: 1529729125 
   {% endhighlight %}

THe elastic container registry hosts our docker images which contains our  with JS Application. You will not have access to Push to the repo that currently is being used; in order to run this on your own you will need to create an ecr repo and update the following files:
#### .gitlab-ci.yml
{% highlight yaml %} 
variables:
   DOCKER_REGISTRY: public.ecr.aws/xxxxx**
   AWS_DEFAULT_REGION: us-east-1
   APP_NAME: liatrio
{% endhighlight %}

#### /terraform/eks/kubernetes.tf
{% highlight yaml %} 
spec {
         container {
         **image = "public.ecr.aws/xxxxxx/liatrio:latest"**
         name  = "liatrio-container"
         port {
            container_port = 5000
         }
   }
}
{% endhighlight %}

## Git Repository
Gitlab was choosen as the git repository. The decision was based on its ability to to both host our code as well as run our CI. Our repositoy structure is as follows:
{% highlight yaml %} 
liatrio-demo {
   - liatrio-demo-eks
   - liatrio-demo-ecs
   - liatrio-demo-documentation
}
{% endhighlight %}

### liatrio-demo-eks
This repository hosts all our terra form files needed to provision our k8 cluster as well as other aws components.

### liatrio-demo-ecs
This repository hosts our js files for are application as well as our docker file which containerized our application so it can be hosted on our eks cluster.

### liatrio-demo-documentation
This is a simple jekyll app which is hosted on gitlab pages; this is where you curently are.

## CI
As stated above gitlab is also used for contiuous integration. .gitlab-ci.yml files are used to drive automation the file in each repositoy will be discussed below.
### eks
The .gitlab-ci.yml file is used to call other gitlab templates which perform our CD tasks by using a gitlab runner with terraform hosted on it.There are SAST tests which run agaist our terraform files to ensure there our no errors prior to begining.

### ecr
The ci files builds our docker image and pushes it to our ecr registry. Every time chages are made to the main branch a new container image will be pushed to our ecr registry.
